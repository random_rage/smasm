# smasm
SPO Machine Assembler

Кросс-ассемблер для модели вычислительной машины [SPOM](https://bitbucket.org/random_rage/spom).

## Использование

`smasm <Имя файла с исходным кодом>`

Подробнее: [wiki](https://bitbucket.org/random_rage/spom/wiki)
